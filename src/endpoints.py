'''
Created on May 5, 2017

@author: Varela
'''

import pandas as pd   
import numpy as np
from os.path import isfile

def get_refused(df):
#Defines if doesn't exist otherwise retrieve
    path = _prefix() + 'refused.csv'
    if isfile(path):
        refuseddf = pd.read_csv(path, sep=';', encoding='utf-8')
    else:
        refuseddf = refused(df, path) 
    return refuseddf

def get_pending(df):
#Defines if doesn't exist otherwise retrieve
    path = _prefix() + 'pending.csv'
    if isfile(path):
        pendingdf = pd.read_csv(path, sep=';', encoding='utf-8')
    else:
        pendingdf = pending(df, path)
    return pendingdf  
    
def get_cancelled(df):
#Defines if doesn't exist otherwise retrieve
    path = _prefix() + 'cancelled.csv'
    if isfile(path):
        cancelleddf = pd.read_csv(path, sep=';', encoding='utf-8')
    else:
        cancelleddf = pending(df, path)
    return cancelleddf

def get_oaccepted(df):
#Defines if doesn't exist otherwise retrieve
    path = _prefix() + 'oaccepted.csv'
    if isfile(path):
        oaccepteddf = pd.read_csv(path, sep=';', encoding='utf-8')
    else:
        oaccepteddf = oaccepted(df, path)
    return oaccepteddf
    
def refused(df,save=True):
    refusedary     = df[ (df['Activity'] == 'O_Refused') | (df['Activity'] == 'A_Denied') ]['Case ID'].unique()
    l = refusedary.shape[0]
    resultdf     = pd.DataFrame(data=refusedary,index=range(0,l), columns=['Case ID'])  
    
    print l  
    if save: 
        path = _prefix() + 'refused.csv'
        resultdf.to_csv(path, sep=';', encoding='utf-8')
        
    return resultdf     

def pending(df, savepath =''):
    pendingary     = df[ (df['Activity'] == 'A_Pending') ]['Case ID'].unique()
    l = pendingary.shape[0]
    resultdf     = pd.DataFrame(data=pendingary,index=range(0,l), columns=['Case ID'])
    
    print l  
    if savepath:         
        resultdf.to_csv(savepath, sep=';', encoding='utf-8')
                
    return resultdf

def cancelled(df,save=True):
    cancelledary     = df[ (df['Activity'] == 'O_Cancelled') ]['Case ID'].unique()
    l = cancelledary.shape[0]
     
    resultdf     = pd.DataFrame(data=cancelledary,index=range(0,l), columns=['Case ID'])
    pendingdf    = pending(df, False)
    refuseddf    = refused(df, False)
    
    resultdf    = resultdf[~resultdf['Case ID'].isin(set(pendingdf['Case ID']))]
    resultdf    = resultdf[~resultdf['Case ID'].isin(set(refuseddf['Case ID']))]
    
    print l   
    if save: 
        path = _prefix() + 'cancelled.csv'        
        resultdf.to_csv(path, sep=';', encoding='utf-8')        
    return resultdf                                

def oaccepted(df,save=True):
    oacceptedary     = df[ (df['Activity'] == 'O_Accepted') ]['Case ID'].unique()
    l = oacceptedary.shape[0]
     
    resultdf     = pd.DataFrame(data=oacceptedary,index=range(0,l), columns=['Case ID'])
    
    
    print l   
    if save: 
        path = _prefix() + 'oaccepted.csv'        
        resultdf.to_csv(path, sep=';', encoding='utf-8')        
    return resultdf                                

def _prefix():
    return '../bpi2017/src/endpoints/'