import pandas as pd   
import numpy as np
import stats  as sts
import endpoints as pts
# from db import case_create, order_create 
# path        = '../bpi2017/src/resolved/Without incomplete cases-BPI Challenge 2017.csv'
# df          = pd.read_csv(path, delimiter=';', encoding='utf-8-sig')

# reqdf =   sts.request_amount(df, '../bpi2017/src/statistics/amount.csv' )
# loandf =  sts.loan_goal(df, '../bpi2017/src/statistics/loan_goal.csv' )
# typedf =  sts.application_type(df, '../bpi2017/src/statistics/application_type.csv' )
# 
# pendingdf  = pts.pending(df, True)
# refuseddf  = pts.refused(df, True)
# cancelleddf = pts.cancelled(df, True)
# 
# endpointsdf   = sts.endpoints(df, True)
# traindf, testdf = case_create(df)
# otraindf, otestdf = order_create(df)

df = pd.read_table('../bpi2017/src/resolved/AllEventInfoInOneLine.csv', delimiter=',')
print df.head(1)
print df.shape
df = df.iloc[:,67:]
print df.shape
print df.head(1)
print df.columns[:13]

activitiesdf = df.iloc[:,13:]
uniq = []
for col in activitiesdf.columns:
    act = activitiesdf.loc[:,col]
    act = act[~act.isnull()]
    act = act.unique()
    uniq = set(list(uniq) + list(act))

uniq = list(uniq)
uniq.sort()
print uniq

print len(uniq)

testdf     = df.iloc[:10000,:]
#testdf     = df
newcolumns = list(testdf.columns[:13]) + uniq + ['s0', 's1']
zeros      = [0]*(len(uniq)+2)
datasets   = {}
is0 = newcolumns.index('s0') 
is1 = is0+1
c = 0  
for index, row in testdf.iterrows():
    thisdata = list(row[:13]) + list(zeros)
    c+=1
    if c % 100 == 0:
        print "Applications processed", c
    for c in xrange(13,77):
        thisstate = row[c]
        if pd.isnull(row[c]):
            break
        else:            
            if pd.isnull(row[c+1]):
                nextstate = 'f'
            else:
                nextstate = row[c+1]
            #overwrite previous s0, s1
            thisdata[is0] = thisstate
            thisdata[is1] = nextstate            
            thispandasdata = np.array(thisdata).reshape((1,41))
            if datasets.has_key(thisstate):
                thisdataset = datasets[thisstate]                
                l = thisdataset.shape[0]
                thisdataset.loc[l,newcolumns] = thispandasdata
            else:                
                thisdataset = pd.DataFrame(data=thispandasdata, columns=newcolumns)       
            datasets[thisstate] = thisdataset
            #update thisstate column count
            i = newcolumns.index(thisstate)
            thisdata[i] +=1
           

#printing output
for key, dataset in datasets.iteritems():            
    print key, dataset.shape




