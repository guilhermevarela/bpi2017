'''
Created on May 8, 2017

@author: Varela
'''
import pandas as pd
import numpy as np
from endpoints import get_cancelled, get_pending, get_refused 


def request_amount(df, path):
    tempdf  = df[df['Activity']=='A_Create Application']
    tempdf  = tempdf[[ '(case) RequestedAmount','Case ID']]

          
    df1 = amount_lessthen(tempdf, 100, 'Amount Range')
    df2 = amount_between(tempdf, 100, 10000, 'Amount Range')
    df3 = amount_between(tempdf, 10000, 20000, 'Amount Range')
    df4 = amount_greaterorequal(tempdf, 20000, 'Amount Range')
    
    resultdf  = df1.append(df2, ignore_index=True).append(df3, ignore_index=True).append(df4, ignore_index=True)    
    resultdf  = resultdf.groupby('Amount Range')['(case) RequestedAmount'].agg(['sum', 'count', 'mean', 'std'])
    print resultdf.head() 
    if path:
        resultdf.to_csv(path, sep=';', encoding='utf-8')
    return resultdf   
        
    
        
def amount_lessthen(df, ub, f_new):
    tempdf  = df[df['(case) RequestedAmount'] < ub]
    
    v_new = "0-" + str(ub) 
    l = tempdf['(case) RequestedAmount'].count()
    newdata = [v_new]*l
    df2   = pd.DataFrame(data=newdata, columns=[f_new], index=tempdf.index)
    
    result = pd.concat([tempdf,df2], axis=1,  join_axes=[tempdf.index])

    print result.head(5)
    return result
 
def amount_greaterorequal(df, lb, f_new):        
    tempdf  = df[df['(case) RequestedAmount'] >= lb]
         
    v_new = '>=' + str(lb)   
    l = tempdf['(case) RequestedAmount'].count()
    newdata = [v_new]*l
    df2   = pd.DataFrame(data=newdata, columns=[f_new], index=tempdf.index)
    result = pd.concat([tempdf,df2], axis=1,  join_axes=[tempdf.index])
    print result.head(5)
    return result

def amount_between(df, lb, ub, f_new):

    tempdf = df[(df['(case) RequestedAmount'] >= lb) & (df['(case) RequestedAmount'] < ub)]

    
    v_new =  str(lb) + ' - ' + str(ub)   
    l       = tempdf['(case) RequestedAmount'].count()
    newdata = [v_new]*l
    df2    = pd.DataFrame(data=newdata, columns=[f_new], index=tempdf.index)
    result = pd.concat([tempdf,df2], axis=1,  join_axes=[tempdf.index])
    print result.head(5)
    return result        
            
def loan_goal(df, path=''):
    tempdf  = df[df['Activity']=='A_Create Application']
    goaldf  = tempdf[['Case ID','(case) LoanGoal']].groupby('(case) LoanGoal')['Case ID'].count()
    print goaldf
    if path: 
        goaldf.to_csv(path, sep=';', encoding='utf-8-sig')
    return goaldf

def application_type(df, path=''):    
    tempdf  = df[df['Activity']=='A_Create Application']
    tempdf  = tempdf[['Case ID','(case) ApplicationType', '(case) LoanGoal', '(case) RequestedAmount']]
    typedf  = tempdf[['Case ID','(case) ApplicationType']].groupby('(case) ApplicationType')['Case ID'].count()
 
    print typedf
    if path: 
        typedf.to_csv(path, sep=';', encoding='utf-8-sig')
    return typedf

def endpoints(df, save=True):
     
    
    pendingdf = get_pending(df)
    p = len(pendingdf['Case ID'])
    
    refuseddf = get_refused(df)
    r = len(refuseddf['Case ID'])
        
    cancelleddf  = get_cancelled(df)
    c = len(cancelleddf['Case ID'])
    
    print p
    print r
    print c
    print "-----------------------"
    print (p + r + c)
           
    newdata = np.array([['Cancelled',c],['Pending',p],['Refused',r]])       
    resultdf= pd.DataFrame(data=newdata, columns=['Status', 'Count'], index=range(0,3))                         
    if save:
        endpointspath = '../bpi2017/src/statistics/endpoints.csv'
        resultdf.to_csv(endpointspath , sep=';', encoding='utf-8-sig')
    return resultdf    