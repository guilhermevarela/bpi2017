'''
Created on May 9, 2017

@author: Varela
'''

import pandas as pd  
from endpoints import get_cancelled, get_pending, get_refused, get_oaccepted

def case_create(df, fractrain=0.9):
    #  random samples of array
    casefields = ['Case ID', '(case) RequestedAmount', '(case) ApplicationType', '(case) LoanGoal']
    tempdf = df[df['Activity'] == 'A_Create Application']
    tempdf = tempdf[casefields]
    idx  =  tempdf.index 
    l = len(idx)
    print l
    
    cancelleddf = get_cancelled(df)
    cancellist  = list(cancelleddf['Case ID'])
    cancelleddf  = newcategory(tempdf, cancellist, 'Status', 'Cancelled')    
    trainadf, testadf =   randomsample(cancelleddf,fractrain)
    
    refuseddf   = get_refused(df)
    refusedlist  = list(refuseddf['Case ID'])
    refuseddf  = newcategory(tempdf, refusedlist, 'Status', 'Refused')
    trainbdf, testbdf =   randomsample(refuseddf,fractrain)
    
    pendingdf   = get_pending(df)
    pendinglist  = list(pendingdf['Case ID'])
    pendingdf  = newcategory(tempdf, pendinglist, 'Status', 'Pending')
    traincdf, testcdf =   randomsample(pendingdf,fractrain)
        
    traindf = trainadf.append(trainbdf, ignore_index=False).append(traincdf, ignore_index=False)
    testdf  = testadf.append(testbdf, ignore_index=False).append(testcdf, ignore_index=False)
    
    trainpath = _path_prefix() + 'case/train.csv'
    traindf.to_csv(trainpath, sep=';', encoding='utf-8')
    
    testpath  = _path_prefix() + 'case/test.csv'
    testdf.to_csv(testpath, sep=';', encoding='utf-8')
    return traindf, testdf 
   
def newcategory(df, caseidlist, newlabel, newvalue):
    
    tempdf = df[df['Case ID'].isin(caseidlist)] 
    l = len(tempdf['Case ID'])
    newdata = [newvalue]*l 
    
    newdf     = pd.DataFrame(data=newdata, columns=[newlabel], index=tempdf.index)
    resultdf  = pd.concat([tempdf,newdf], axis=1,  join_axes=[tempdf.index])
    print resultdf.head()
    return resultdf 

def newcategory2(df, itemlist, field, newlabel, newvalue):
    
    tempdf = df[df[field].isin(itemlist)]
     
    l = len(tempdf[field])
    newdata = [newvalue]*l 
    
    newdf     = pd.DataFrame(data=newdata, columns=[newlabel], index=tempdf.index)
    resultdf  = pd.concat([tempdf,newdf], axis=1,  join_axes=[tempdf.index])
    print resultdf.head()
    return resultdf 


def randomsample(df,fractrain=0.9):
    testdf  = df.sample(frac=(1-fractrain), replace=False, random_state=123)
    
    idx     = list(set(df.index) - set(testdf.index)) 
    traindf = df[df.index.isin(idx)] 
    
    return traindf, testdf 
     

def _path_prefix():
    return '../bpi2017/src/db/'     

def order_create(df, fractrain=0.9):
    casefields   = ['Case ID', 'Activity', '(case) RequestedAmount', '(case) ApplicationType', '(case) LoanGoal']     
    offerfields  = ['Accepted', 'CreditScore', 'EventID', 'FirstWithdrawalAmount', 'MonthlyCost', 'NumberOfTerms', 'OfferedAmount', 'Selected'] 
    acceptedfields = ['OfferID']
    
    fields = casefields + offerfields + acceptedfields
    
    
#     activities  = ['O_Created Offer','O_Refused', 'O_Accepted', 'O_Denied']
        
    tempdf = df[fields]
    
    cancelleddf =   tempdf[tempdf['Activity'] == 'O_Cancelled']
    refuseddf   =   tempdf[(tempdf['Activity'] == 'O_Refused')   | (tempdf['Activity'] == 'O_Denied')]    
    accepteddf  =   tempdf[tempdf['Activity'] == 'O_Accepted']
    
    
    cancelledlist  = list(cancelleddf['OfferID'])
    cancelleddf    = newcategory2(tempdf, cancelledlist, 'EventID', 'OStatus', 'OCancelled')
    trainadf, testadf =   randomsample(cancelleddf,fractrain)
    
    refusedlist  = list(refuseddf['OfferID'])
    refuseddf   = newcategory2(tempdf,   refusedlist, 'EventID', 'OStatus', 'ORefused')
    trainbdf, testbdf =   randomsample(refuseddf, fractrain)
    

    acceptedlist = list(accepteddf['OfferID'])
    accepteddf  = newcategory2(tempdf, acceptedlist, 'EventID', 'OStatus', 'OAccepted')
    traincdf, testcdf =   randomsample(accepteddf,fractrain)   
          
    traindf = trainadf.append(trainbdf, ignore_index=False).append(traincdf, ignore_index=False)
    testdf  = testadf.append(testbdf, ignore_index=False).append(testcdf, ignore_index=False)
    
    fields.remove('Activity')
    fields.remove('OfferID')
    fields.remove('EventID') 
    fields    = fields + ['OStatus']
    trainpath = _path_prefix() + 'orders/train.csv'    
    traindf   = traindf[fields]
    traindf.to_csv(trainpath, sep=';', encoding='utf-8')
    
    testpath  = _path_prefix() + 'orders/test.csv'
    testdf     = testdf[fields]
    testdf.to_csv(testpath, sep=';', encoding='utf-8')
    return traindf, testdf 
     

   
    
    
    
