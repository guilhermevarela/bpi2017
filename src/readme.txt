Relevant activities:

activity:
O_Create Offer

attributes: 
Accepted, Credit Score, EventID, FirstWithdrawalAmount, MonthlyCost, NumberOfTerms, OfferID, OfferedAmount, Selected

activity: 
O_Accepted

attributes: 
OfferID == EventID of O_Create Offer
 
activity: 
O_Refused

attributes: 
OfferID == EventID of O_Create Offer
 