import numpy as np
import pandas as pd
def seconds2hours(secs):
    hours, remainder = divmod(secs, 3600)
    minutes, seconds = divmod(remainder, 60)
    x = hours + minutes/60 + seconds/3600
    return x

def hours2days(hours):
    days, remainder = divmod(hours, 24)
    x = days + remainder/24

def seconds2days(secs):
    x = seconds2hours(secs)
    x = hours2days(x)


def loaddf_workitems():
  actynames = [
             'W_Validate application','W_Call after offers', 'W_Assess potential fraud wrapper',
             'W_Call incomplete files', 'W_Complete application', 'W_Handle leads',
             'W_Personal loan collection', 'W_Shortened completion'
            ]

  wfiles = [
         'w_validate_application_wrapper.csv', 'w_call_after_offers_wrapper.csv',
         'w_assess_potential_fraud_wrapper.csv','w_call_incomplete_files_wrapper.csv',
         'w_complete_application_wrapper.csv', 'w_handle_leads_wrapper.csv',
         'w_personal_loan_collection_wrapper.csv', 'w_shortened_completion_wrapper.csv'
          ]
  ifiles = [
        'w_validate_application_instantaneous.csv', 'w_call_after_offers_instantaneous.csv',
         'w_assess_potential_fraud_instantaneous.csv', 'w_call_incomplete_files_instantaneous.csv',
         'w_complete_application_instantaneous.csv','w_handle_leads_instantaneous.csv',
         'w_personal_loan_collection_instantaneous.csv', 'w_shortened_completion_instantaneous.csv'
        ]

  # ON BOTH FILES THERE IS DATA MIXED
  #SUCCESS - W_* with excuting time
  #DELAYS -  W_* with no excuting time, but transition time is greater than a second
  #FILLERS - W_* with no excuting time or transition time
  #MAKE A BINDING DATABASE
  isfirst = True
  for i, acty in enumerate(actynames):
      df1 = pd.read_csv(wfiles[i], delimiter=',', index_col=0, encoding='utf-8-sig')
      df2 = pd.read_csv(ifiles[i], delimiter=',', index_col=0, encoding='utf-8-sig')

      #df1 -> DELAYS -> df3
      df3 = df1.drop_duplicates(subset='Case ID', keep=False)
      index = df1.index.difference(df3.index)
      df1 = df1.loc[index,:]

      print acty,(df1.shape[0] + df2.shape[0] + df3.shape[0])

      #df2 -> DELAYS -> df3
      index = pd.to_timedelta(df2['Delta']) > pd.Timedelta('1 Second')
      df3 = pd.concat((df3,df2[index]),axis=0)
      df2 = df2[~index]
      print acty, df1.shape[0] + df2.shape[0] + df3.shape[0]

      df1["Status"] = 1 # success
      df2["Status"] = 2 # filler
      df3["Status"] = 3 # delay

      df1["owner"] = acty
      df2["owner"] = acty
      df3["owner"] = acty

      if isfirst:
          df = pd.concat((df1,df2,df3),axis=0)
          isfirst = False
      else:
          df = pd.concat((df,df1,df2,df3),axis=0)


  print 'Total', df.shape[0]
  return df


def filter_milestones(df, start, finish, leaks=[], check=False):
  # inner join of start to finish, leaks
  # From leaks exclude instances that did reach finish
  # RULE: startdf.shape[0] = milestonedf.shape[0]
  # endpoint = finish + leaks ( no rep )
  #1 Data wraggling
  # 1.A - masks for activities
  maskstart  = df['Activity'].isin(start)
  maskfinish = df['Activity'].isin(finish)



  startdf      = df.loc[maskstart]
  finishdf     = df.loc[maskfinish]


  # 1.B - gets one instance for O_ latest is truer
  mask = startdf.duplicated(subset='Case ID', keep='last')
  startdf = startdf.loc[(~mask)]

  mask = finishdf.duplicated(subset='Case ID', keep='last')
  finishdf = finishdf.loc[(~mask)]


  # 1.C - rename columns on endpoints before merge
  joinedcolumns = ['Case ID','Endpoint','Endpoint Timestamp']
  finishdf.columns = joinedcolumns


  #2 Merge start2finishdf
  # Keep startindex :O should be left_index=True
  milestonedf  = startdf.merge(finishdf, how='inner', on='Case ID', right_index=True)


  #3 Ajust for leaks
  if leaks:
    maskleaks= df['Activity'].isin(leaks)

    leaksdf = df.loc[maskleaks]
    mask = leaksdf.duplicated(subset='Case ID', keep='last')
    leaksdf = leaksdf.loc[(~mask)]
    leaksdf.columns = joinedcolumns

    leaksindex  = startdf.index.difference(milestonedf.index)
    # Keep startindex :O should be left_index=True
    start2leaksdf = startdf.loc[leaksindex].merge(leaksdf, how='inner', on='Case ID', right_index=True)

    milestonedf = pd.concat((milestonedf, start2leaksdf),axis=0)

  milestonedf.drop('Activity', axis=1, inplace=True)

  #4- Compute a new Delta column
  startseries         = pd.to_datetime(milestonedf['Start Timestamp'])
  endpointseries      = pd.to_datetime(milestonedf['Endpoint Timestamp'])

  milestonedf['Delta'] = ( endpointseries - startseries).astype('timedelta64[ns]')

  #5 - Rename endpoints
  milestonedf.loc[:,'Endpoint'] = milestonedf['Endpoint'].str.replace('A_','')
  milestonedf.loc[:,'Endpoint'] = milestonedf['Endpoint'].str.replace('O_','')

  if check:
    buf = "Check start[%d] == finish + leaks[%d]" % (startdf.shape[0],milestonedf.shape[0])
    print buf
    buf = "Check Case ID duplicates? [%d]" % (milestonedf.duplicated(subset='Case ID').sum(axis=0))
    print buf

  return milestonedf

def builddf_transition(df, timestampcolumn='event_time.timestamp', casecolumn='case_concept.name'):
  #keepcols   = ['Case ID', 'Activity', 'Start Timestamp', 'Complete Timestamp']
  #outputcols = ['Case ID', 'Activity', 'Start Timestamp', 'Complete Timestamp', 'Transition', 'Exec']

  #Data wrangling 1 - finding case
  caseseries0 = df.loc[:,casecolumn]
  caseseries1 = caseseries0.shift(1)
  mask        = caseseries0 == caseseries1

  #Data wrangling 2 - computing transition
  timestampseries0 = pd.to_datetime( df.loc[:,timestampcolumn] )
  timestampseries1 = timestampseries0.shift(1)


  df.loc['delta']  = (timestampseries1-timestampseries0).astype('timedelta64[s]')
  df.loc[~mask, 'delta'] = 0

  return df



